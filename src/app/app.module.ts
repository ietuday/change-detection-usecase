import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent, Wrapper } from './app.component';
import { FormsModule } from '@angular/forms';
import { HelloComponent } from './hello.component';

import { MyDirective } from './structural.directive';
@NgModule({
  declarations: [
    AppComponent, 
    HelloComponent , 
    MyDirective, 
    Wrapper
  ],
  imports: [
    BrowserModule,
    FormsModule       
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
