import { Component, Input, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';

@Component({
  selector: 'hello',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `<h1>Hello {{name}}! {{ abc }} </h1>`,
  styles: [`h1 { font-family: Lato; }`]
})
export class HelloComponent  {
  @Input() name: string;

  lul = 'kek';

  constructor(private changeDetectorRef: ChangeDetectorRef) {}
  
  changeState(newState) {
    this.lul = newState;
    this.changeDetectorRef.markForCheck();
  }

  get abc() {
    console.log('getter called');
    return this.lul;
  }
}
