import { Component, ChangeDetectorRef, ViewChild } from '@angular/core';
import { HelloComponent } from './hello.component';
import { MyDirective } from './structural.directive';
import { of } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  name = 'Angular';

  @ViewChild(HelloComponent) hello: HelloComponent;
  @ViewChild(MyDirective) myDirective: MyDirective;

  cond = false;

  dataSource = {
    get: () => {
      return {
        data$: of(['a', 'b', 'c'])
      }
    }
  }

  constructor(private changeDetectorRef: ChangeDetectorRef) {
    setTimeout(() => {
      this.myDirective.create();
    }, 3000);

    setTimeout(() => {
      console.log('do detect changes');
      this.hello.changeState('new value');
      this.changeDetectorRef.detectChanges();
    }, 6000);
  }
}           

@Component({
  selector: 'wrapper',
  template: '<ng-content></ng-content>'
})
export class Wrapper {

}
