import {
    ChangeDetectorRef,
    ComponentFactoryResolver,
    ComponentRef,
    Directive,
    EmbeddedViewRef,
    forwardRef,
    Input,
    TemplateRef,
    ViewContainerRef
  } from '@angular/core';
  
  @Directive({
    selector: '[myDirective]'
  })
  export class MyDirective {
    context = {
      $implicit: null
    }
    constructor(
      public template: TemplateRef<any>,
      public viewContainer: ViewContainerRef,
      private changeDetectorRef: ChangeDetectorRef
    ) {
  
    }
  
    @Input() myDirectiveOf;
  
    newView;
  
    create() {
      this.newView = this.viewContainer.createEmbeddedView(this.template, this.context);
      debugger;
  
      const session = this.myDirectiveOf.get();
      session.data$.subscribe(data => {
        this.context.$implicit = this.context.$implicit ? [...this.context.$implicit, ...data] : data;
        this.changeDetectorRef.detectChanges();
      });
    }
  }